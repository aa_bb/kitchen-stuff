#include "Header.h"
using namespace std;

void MyApp::printMenu() {
	cout << "�������� �������� ��������:" << endl;
	cout << "0 - ��������� ������" << endl;
	cout << "1 - �������� ������" << endl;
	cout << "2 - ������� ������" << endl;
	cout << "3 - ������� �� ����� ��� �������� ������" << endl;
}

MyApp::MyApp(AvlBinTree* _tree) {
	tree = _tree;
	setlocale(LC_CTYPE, "Russian");
}

void MyApp::printMessage(string message) {
	cout << message << endl;
}

void MyApp::printMessage(string message, bool mode) {
	if (mode)
		cout << message << endl;
	else
		cout << message;
}

void  MyApp::insertKitchenStuff() {
	int choice;
	cout << "�������� ��� �������, ������� �� ������ ��������:" << endl;
	cout << "1 - �����" << endl;
	cout << "2 - ��������" << endl;
	cout << "3 - ������������� �����" << endl;
	cout << "4 - ������� �����" << endl;
	cout << "5 - �����������" << endl;
	cin >> choice;

	KitchenStuffItem* item;
	int id, r, g, b, power;
	double volume, gasExpense;
	bool hasPressurecookerMode;

	switch (choice) {
	case 1:
		cout << "������� id, ������������� ��������, ������������� �������� � ������������� ������" << endl;
		cin >> id >> r >> g >> b;
		item = new Stove(id, r, g, b);
		break;
	case 2:
		cout << "������� id � ����� ��������" << endl;
		cin >> id >> volume;
		item = new Pan(id, volume);
		break;
	case 3:
		cout << "������� id, ������������� ��������, ������������� ��������, ������������� ������ " <<
			"� ��������" << endl;
		cin >> id >> r >> g >> b >> power;
		item = new EStove(id, r, g, b, power);
		break;
	case 4:
		cout << "������� id, ������������� ��������, ������������� ��������, ������������� ������ " <<
			"� ������ ����" << endl;
		cin >> id >> r >> g >> b >> gasExpense;
		item = new GasStove(id, r, g, b, gasExpense);
		break;
	case 5:
		cout << "������� id, ������������� ��������, ������������� ��������, ������������� ������, " <<
			"��������, ����� � ������� ������ ���������� (1-����, 0-���)" << endl;
		cin >> id >> r >> g >> b >> power >> volume >> hasPressurecookerMode;
		item = new Multicooker(id, r, g, b, power, volume, hasPressurecookerMode);
		break;
	default:
		cout << "������ ������ ���, �� ������ ���������� � ����" << endl;
		return;
	}
	tree->insertItem(item);
	tree->print();
}

void  MyApp::deleteKitchenStuff() {
	cout << "������� id �������, ������� ������ �������" << endl;
	int id;
	cin >> id;
	tree->deleteItem(id);
	tree->print();
}

void  MyApp::printKitchenStuff() {
	cout << "�������� �������� �������:" << endl;
	cout << "1 - ������ �����" << endl;
	cout << "2 - ������������ �����" << endl;
	cout << "3 - �������� �����" << endl;
	int choice;
	cin >> choice;
	switch (choice) {
	case 1:
		tree->preOrderPrint();
		break;
	case 2:
		tree->inOrderPrint();
		break;
	case 3:
		tree->postOrderPrint();
		break;
	default:
		cout << "������ ������ ���, �� ������ ���������� � ����" << endl;
		return;
	}
}

void  MyApp::start() {
	cout << "����� ���������� � ���������� Kitchen Stuff!" << endl;
	int choice;
	while (true) {
		printMenu();
		cin >> choice;
		switch (choice) {
		case 0:
			cout << "���������� ��������� ���������" << endl;
			return;
			break;
		case 1:
			insertKitchenStuff();
			break;
		case 2:
			deleteKitchenStuff();
			break;
		case 3:
			printKitchenStuff();
			break;
		default:
			cout << "������ ������ ���� ���, ���������� ��� ���" << endl;
			break;
		}
	}
}

TNode::~TNode() {
	delete Data;
}

void AvlBinTree::addNode(PNode& currentNode, KitchenStuffItem* item) {
	if (currentNode == nullptr) {
		currentNode = new TNode;
		currentNode->Data = item;
		currentNode->Height = 1;
		currentNode->Right = currentNode->Left = nullptr;
	}

	else if (item->get_id() == currentNode->Data->get_id()) {
		MyApp::printMessage("����� ������� ��� ����������");
		currentNode->Data->print(true);
	}

	else {
		if (item->get_id() < currentNode->Data->get_id())
			addNode(currentNode->Left, item);
		if (item->get_id() > currentNode->Data->get_id())
			addNode(currentNode->Right, item);
		fixheight(currentNode);
		balance(currentNode);
	}
}

void AvlBinTree::balance(PNode& currentNode) {
	if (bfactor(currentNode) == -2) {
		if (bfactor(currentNode->Left) > 0) {
			RR(currentNode->Left);
			LL(currentNode);
		}
		else {
			LL(currentNode);
		}
	}

	else if (bfactor(currentNode) == 2) {
		if (bfactor(currentNode->Right) < 0) {
			LL(currentNode->Right);
			RR(currentNode);
		}
		else {
			RR(currentNode);
		}
	}
}

void AvlBinTree::fixheight(PNode currentNode) {
	int lh = height(currentNode->Left);
	int rh = height(currentNode->Right);
	currentNode->Height = max(lh, rh) + 1;
}

int AvlBinTree::height(PNode currentNode) {
	if (currentNode == NULL)
		return 0;
	else return currentNode->Height;
}

int AvlBinTree::bfactor(PNode currentNode) {
	return height(currentNode->Right) - height(currentNode->Left);
}

void AvlBinTree::LL(PNode& currentNode) {
	PNode tmp = currentNode->Left;
	currentNode->Left = tmp->Right;
	tmp->Right = currentNode;
	currentNode = tmp;
	fixheight(currentNode->Right);
	fixheight(currentNode);
}

void AvlBinTree::RR(PNode& currentNode) {
	PNode tmp = currentNode->Right;
	currentNode->Right = tmp->Left;
	tmp->Left = currentNode;
	currentNode = tmp;
	fixheight(currentNode->Left);
	fixheight(currentNode);
}

void AvlBinTree::deleteNode(PNode& currentNode, int id) {
	if (currentNode == nullptr) {
		MyApp::printMessage("������� � ����� id ���");
		return;
	}

	else if (id < currentNode->Data->get_id())
		deleteNode(currentNode->Left, id);
	else if (id > currentNode->Data->get_id())
		deleteNode(currentNode->Right, id);
	else {
		if (currentNode->Left == nullptr) {
			PNode tmp = currentNode;
			currentNode = currentNode->Right;
			delete tmp;
			return;
		}
		else if (currentNode->Right == nullptr) {
			PNode tmp = currentNode;
			currentNode = currentNode->Left;
			delete tmp;
			return;
		}
		else { // ����� ������ ������ � ������ ���������
			int fd;
			findReplacer(currentNode->Right, fd);
			currentNode->Data->put_id(fd);
		}
	}
	fixheight(currentNode);
	balance(currentNode);
}

void AvlBinTree::findReplacer(PNode& currentNode, int& fd) {
	if (currentNode->Left != NULL) // ���� min (����� �����)
		findReplacer(currentNode->Left, fd);
	else {
		fd = currentNode->Data->get_id();
		currentNode = currentNode->Right;
		return;
	}
	fixheight(currentNode);
	balance(currentNode);
}

void AvlBinTree::preOrderPrint(PNode currentNode) { // ������ �����
	if (currentNode != nullptr) {
		currentNode->Data->print(true);
		preOrderPrint(currentNode->Left);
		preOrderPrint(currentNode->Right);
	}
}

void AvlBinTree::inOrderPrint(PNode currentNode) { // ������������ �����
	if (currentNode != nullptr) {
		inOrderPrint(currentNode->Left);
		currentNode->Data->print(true);
		inOrderPrint(currentNode->Right);
	}
}

void AvlBinTree::postOrderPrint(PNode currentNode) { // �������� �����
	if (currentNode != nullptr) {
		postOrderPrint(currentNode->Left);
		postOrderPrint(currentNode->Right);
		currentNode->Data->print(true);
	}
}

void AvlBinTree::clearTree(PNode currentNode) {
	if (currentNode != nullptr) {
		clearTree(currentNode->Left);
		clearTree(currentNode->Right);
		delete currentNode;
	}
}

AvlBinTree::AvlBinTree() {
	root = nullptr;
}

AvlBinTree::~AvlBinTree() {
	clearTree(root);
	MyApp::printMessage("������ ������� �������");
}

void AvlBinTree::insertItem(KitchenStuffItem* item) {
	addNode(root, item);
}

void AvlBinTree::deleteItem(int id) {
	deleteNode(root, id);
}

void AvlBinTree::preOrderPrint() {
	MyApp::printMessage("������ �����:");
	preOrderPrint(root);
}

void AvlBinTree::inOrderPrint() {
	MyApp::printMessage("������������ �����:");
	inOrderPrint(root);
}

void AvlBinTree::postOrderPrint() {
	MyApp::printMessage("�������� �����");
	postOrderPrint(root);
}

void AvlBinTree::print() {
	MyApp::printMessage("������:");
	print_tree(root, 0);
}

void AvlBinTree::print_tree(PNode root, int level) {
	if (root != nullptr) {
		print_tree(root->Right, level + 1);
		for (int i = 0; i < level; i++)
			MyApp::printMessage("   ", false);
		MyApp::printMessage(to_string(root->Data->get_id()), true);
		print_tree(root->Left, level + 1);
	}
}

void KitchenStuffItem::print(bool flag) {
	if (flag)
		MyApp::printMessage("id: " + to_string(id));
};

int KitchenStuffItem::get_id() {
	return id;
}

void KitchenStuffItem::put_id(int _id) {
	id = _id;
}

Stove::Stove() {}
Stove::Stove(int _id, int _r, int _g, int _b) {
	id = _id;
	color.red = _r;
	color.green = _g;
	color.blue = _b;
}

void Stove::print(bool flag) {
	KitchenStuffItem::print(flag);
	MyApp::printMessage("color: " + to_string(color.red) + " " + to_string(color.green) + " " +
		to_string(color.blue));
}

Pan::Pan() {}
Pan::Pan(int _id, double _volume) {
	id = _id;
	volume = _volume;
}

void Pan::print(bool flag) {
	KitchenStuffItem::print(flag);
	MyApp::printMessage("volume: " + to_string(volume));
}

EStove::EStove() {}
EStove::EStove(int _id, int _r, int _g, int _b, int _power) {
	id = _id;
	color.red = _r;
	color.green = _g;
	color.blue = _b;
	power = _power;
}

void EStove::print(bool flag) {
	Stove::print(flag);
	MyApp::printMessage("power: " + to_string(power));
}

GasStove::GasStove(int _id, int _r, int _g, int _b, double _gasExpense) {
	id = _id;
	color.red = _r;
	color.green = _g;
	color.blue = _b;
	gasExpense = _gasExpense;
}

void GasStove::print(bool flag) {
	Stove::print(flag);
	MyApp::printMessage("expense of gas: " + to_string(gasExpense));
}

Multicooker::Multicooker(int _id, int _r, int _g, int _b, int _power, double _volume, bool _hasPressurecookerMode) {
	id = _id;
	color.red = _r;
	color.green = _g;
	color.blue = _b;
	power = _power;
	volume = _volume;
	hasPressurecookerMode = _hasPressurecookerMode;
}

void Multicooker::print(bool flag) {
	EStove::print(flag);
	Pan::print(false);
	MyApp::printMessage("has pressurecooker mode? " + to_string(hasPressurecookerMode));
}