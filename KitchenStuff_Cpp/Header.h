#include <algorithm>
#include <iostream>
#include <clocale>
#include <string>

using namespace std;

class KitchenStuffItem {
protected:
	int id;
public:
	virtual ~KitchenStuffItem() {};
	virtual void print(bool flag) = 0;
	int get_id();
	void put_id(int _id);
};

struct TNode {
	KitchenStuffItem* Data;
	int Height;
	TNode* Left;
	TNode* Right;
	~TNode();
};

typedef TNode* PNode;

class AvlBinTree {
private:
	PNode root;

	void addNode(PNode& currentNode, KitchenStuffItem* item);
	void balance(PNode& currentNode);
	void fixheight(PNode currentNode);
	int height(PNode currentNode);
	int bfactor(PNode currentNode);
	void LL(PNode& currentNode);
	void RR(PNode& currentNode);
	void deleteNode(PNode& currentNode, int id);
	void findReplacer(PNode& currentNode, int& fd);
	void preOrderPrint(PNode currentNode);
	void inOrderPrint(PNode currentNode);
	void postOrderPrint(PNode currentNode);
	void clearTree(PNode currentNode);
	void print_tree(PNode root, int level);
public:
	AvlBinTree();
	~AvlBinTree();
	void insertItem(KitchenStuffItem* item);
	void deleteItem(int id);
	void preOrderPrint();
	void inOrderPrint();
	void postOrderPrint();
	void print();
};

class MyApp {
private:
	AvlBinTree* tree;
	void printMenu();

public:
	MyApp(AvlBinTree* _tree);
	static void printMessage(string message);
	static void printMessage(string message, bool mode);
	void insertKitchenStuff();
	void deleteKitchenStuff();
	void printKitchenStuff();
	void start();
};

struct Color {
	int red;
	int green;
	int blue;
};

class Stove : virtual public KitchenStuffItem {
protected:
	Color color;
public:
	Stove();
	Stove(int _id, int _r, int _g, int _b);
	virtual void print(bool flag) override;
};

class Pan : virtual public KitchenStuffItem {
protected:
	double volume;
public:
	Pan();
	Pan(int _id, double _volume);
	virtual void print(bool flag) override;
};

class EStove : public Stove {
protected:
	int power;
public:
	EStove();
	EStove(int _id, int _r, int _g, int _b, int _power);
	virtual void print(bool flag) override;
};

class GasStove : public Stove {
protected:
	double gasExpense;
public:
	GasStove(int _id, int _r, int _g, int _b, double _gasExpense);
	virtual void print(bool flag) override;
};

class Multicooker : public EStove, public Pan {
protected:
	bool hasPressurecookerMode;
public:
	Multicooker(int _id, int _r, int _g, int _b, int _power, double _volume, bool _hasPressurecookerMode);
	virtual void print(bool flag) override;
};